#include <iostream>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>

using namespace std;

int main()
{
    
    auto channel = socket(AF_INET,SOCK_DGRAM,IPPROTO_UDP);
	
    char adr[15];
    short port;
    int i;
    string mes1;
	char mes2[255];
	char mes3[255];
    sockaddr_in x;
    cout<<"Введите собственный адрес:\n";
    cin>>adr;
    x.sin_addr.s_addr=inet_addr(adr);
    cout<<"Введите номер собственного порта:\n";
    cin>>port;
    x.sin_port=htons(port);
    //cout<<x.sin_port<<endl;
    x.sin_family=AF_INET;
    
	
	int value=1;
	setsockopt(channel,SOL_SOCKET,SO_REUSEADDR,(const char*)&value,sizeof(value));
	setsockopt(channel,SOL_SOCKET,SO_BROADCAST,(const char*)&value,sizeof(value));
    int result = bind(channel,(const sockaddr*)&x,sizeof (x));
    
    while (true)
    {
        cout<<"\n1-отправить, 2-принять, 3-прекратить цикл:\n";
        cin>>i;
        if (i==1) {
        cout<<"Введите адрес получателя :\n";
        cin>>adr;
        x.sin_addr.s_addr=inet_addr(adr);
        cout<<"Введите порт получателя :\n";
        cin>>port;
        x.sin_port=htons(port);
        //cout<<x.sin_port<<endl;
        cout<<"Введите сообщение :\n";
        getline(cin,mes1);
		getline(cin,mes1);
        auto result=sendto(channel,mes1.c_str(),mes1.size(),0,(const sockaddr*)(&x),sizeof(x));
        }
        else 
		if (i == 2)
        {	socklen_t source_size = sizeof(x);
			auto result = recvfrom ( channel,mes2,sizeof(mes2),0,(sockaddr*)(&x),&source_size);
			cout<< "Новое сообщение:\n" << mes2<<endl;
			cout<< "Адрес отправителя:" << inet_ntoa(x.sin_addr) << ":" << ntohs(x.sin_port) << endl;
			
			
        }
		else
		if (i == 3)
		{
			break;
			close(channel);
		}
        }
		
    return 0;
}
